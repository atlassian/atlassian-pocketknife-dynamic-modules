package com.atlassian.pocketknife.internal.lifecycle.modules;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.Element;

import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

/**
 *
 */
public final class Kit {
    public static final String NOT_SPECIFIED = "not-specified??";

    private Kit() {
    }

    static String getModuleIdentifier(final Element element) {
        return mkId(element.attributeValue("key"), element.attributeValue("name"));
    }

    static String getModuleIdentifier(final ModuleDescriptor<?> descriptor) {
        return descriptor.getCompleteKey();
    }

    static String pluginIdentifier(final Plugin plugin) {
        return plugin.getKey();
    }

    private static String mkId(final String key, final String name) {
        return defaultIfEmpty(key, NOT_SPECIFIED) + " - " + defaultIfEmpty(name, "");
    }

}
